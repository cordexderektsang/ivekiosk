﻿using UnityEngine;
using System.Collections;
using System.IO;
using VoxelBusters.NativePlugins;
using VoxelBusters.Utility;

public class CaptureAndShareOnClickButtonClass : MonoBehaviour {

	public GameObject uiRoot;
	public UITexture waterMark;
	public AppFlowManager appFlowManager;
	public bool isCaptureFinish = false;
	public bool isShareFinish = false;
	public string filePath;
	public bool isCaptureClicked = false;
	public bool isShareClicked = false;
	private Coroutine saveFilePath; 
	[SerializeField, Header("Mail Sharing Properties")]
	private 	string			m_mailSubject		= "Retail Lab";
	//[SerializeField]
	private 	string			m_plainMailBody		= "Thanks for visiting Retail Lab. " + System.Environment.NewLine + System.Environment.NewLine + " For programme details of HD in Retail & e-Tail Management, please visit :" +  System.Environment.NewLine + "http://www.vtc.edu.hk/admission/tc/programme/ba124035-higher-diploma-in-retail-and-e-tail-management/" + " or "  + System.Environment.NewLine + " http://www.facebook.com/retail.place";
	[SerializeField]
	private 	string			m_htmlMailBody		= "<html><body><h1>Hello</h1></body></html>";
	[SerializeField]
	private 	string[] 		m_mailToRecipients;
	[SerializeField]
	private 	string[] 		m_mailCCRecipients;
	[SerializeField]
	private 	string[] 		m_mailBCCRecipients;



	public void OnClickCapture ()
	{

		if (!isCaptureClicked) {
			Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
			isCaptureFinish = false;
			uiRoot.SetActive (false);
			StartCoroutine (Capture ());
			isCaptureClicked = true;
		}
	}

	public void OnClickShareToSocial ()
	{
		if (!isShareClicked) {
			Debug.Log (filePath);
			isShareFinish = false;
			//StartCoroutine (Share ());
		
				// Create composer
				MailShareComposer	_composer	= new MailShareComposer();
				_composer.Subject				= m_mailSubject;
				_composer.Body					= m_plainMailBody;
				_composer.IsHTMLBody			= false;
				_composer.ToRecipients			= m_mailToRecipients;
				_composer.CCRecipients			= m_mailCCRecipients;
				_composer.BCCRecipients			= m_mailBCCRecipients;
			_composer.AddAttachmentAtPath(filePath, MIMEType.kPNG);

				// Show share view
				NPBinding.Sharing.ShowView(_composer, FinishedSharing);

			isShareClicked = true;
		}
	}


	private void FinishedSharing (eShareResult _result)
	{
		uiRoot.gameObject.SetActive (true);
		isShareFinish = true;
		//appFlowManager.DeletePathForfileImagePath ();
		appFlowManager.TowardPageOne ();
	}

/*	public IEnumerator CaptureAndShare ()
	{
		uiRoot.gameObject.SetActive (false);
		yield return new WaitForSeconds(0.2f);
		Debug.Log ("capture");
		#if !UNITY_EDITOR && UNITY_ANDROID
		AndroidJavaObject activitys = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
		activitys.Call("runOnUiThread", new AndroidJavaRunnable(() =>
		{
		new AndroidJavaClass("android.widget.Toast").CallStatic<AndroidJavaObject>("makeText", activitys, "Loading", 5).Call("show");
		}));
		#endif
		yield return new WaitForSeconds(0);
		CCCaptureAndShare.CaptureAndSaveToAlbum(
			"ARAlphabet",
			(int result) => {
				#if !UNITY_EDITOR && UNITY_ANDROID
				var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
				activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>                                                                       {
				new AndroidJavaClass("android.widget.Toast").CallStatic<AndroidJavaObject>("makeText", activity, "Photo saved to album", 1).Call("show");
				}));
				#endif
				uiRoot.gameObject.SetActive (true);
			}
		);

	}*/

	public IEnumerator Share ()
	{
		uiRoot.gameObject.SetActive (false);
		yield return new WaitForSeconds (0.1f);
		CCCaptureAndShare.callForShareToSocial (
			CCCaptureAndShare.SocialType.SLServiceTypeUIActivity,filePath,
			"CordexAR",
			"Enjoy it!",
			"CordexAR",
			(int result) => {

			
				//uiRoot.gameObject.SetActive (true);
				//isShareFinish = true;
				//appFlowManager.WritePath(filePath);

			}
		);

	}

	public IEnumerator Capture ()
	{
		filePath = "";
		waterMark.alpha = 1;
		yield return new WaitForSeconds(1);
		filePath = CCCaptureAndShare.returnCapturePathAndroidOnly (
			CCCaptureAndShare.SocialType.SLServiceTypeUIActivity,
			"CordexAR",
			"Enjoy it!",
			"CordexAR",
			(int result) => {

				if(saveFilePath == null) {

					saveFilePath = StartCoroutine(captureReady());
				}


			}
		);

	}


	public IEnumerator captureReady ()
	{
		while (true) 
		{
			yield return new WaitForEndOfFrame ();
			if (!string.IsNullOrEmpty (filePath)) 
			{
				waterMark.alpha = 0;
				isCaptureFinish = true;
				//appFlowManager.WritePath (filePath);
				appFlowManager.TowardPageThree ();
				saveFilePath = null;
				break;
			}
		}
	}
}
