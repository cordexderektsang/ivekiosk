﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;


public class AppFlowManager : MonoBehaviour 
{
	
	public ButtonEffect buttonEffect;
	public CaptureAndShareOnClickButtonClass captureAndShareOnClickButtonClass;
	public GameObject uiRoot;
	public UITexture imageDisplay;
	public List<GameObject> appPage = new List<GameObject>();
	public  string fileName = "RetailExperience";
	public  string folderName = "IVE";
	public GameObject shareButton;
	public List<buttonConstructor> rateTexture;

	private string directionPath;
	private string filePath;
	private string fileImagePath;
	private bool isRated = false;
	private string pathForReference;
	private string rate;
	private Coroutine flashStart = null;


	public void Awake ()
	{
		SetUpFile (fileName);
		CreatPathFile ();
	//	StartCoroutine (OnFlashStar());
	}

	/// <summary>
	/// reset toward page one and reset all variable
	/// </summary>
	public void TowardPageOne ()
	{
		for(int i = 0 ; i < rateTexture.Count; i ++)
		{
			rateTexture [i].buttonColour.hover.a = 0.5f;
			rateTexture [i].buttonColour.pressed.a = 0.5f;
			rateTexture [i].thisTexture.alpha = 0.5f;
		}
		appPage [0].SetActive (true);
		appPage [1].SetActive (false);
		appPage [2].SetActive (false);
	//	appPage [3].SetActive (false);
		shareButton.SetActive(false);
		ResetAllNecessaryVariable ();
		rate = "";
	}

	/// <summary>
	/// Toward page two perpare capture
	/// </summary>
	public void OnEnableCamToPageTwo ()
	{
		StartCoroutine(OnClickCamTowardPageTwo ());
	}

	/// <summary>
	/// after perpare capture process capture picture
	/// </summary>
	public void PrepareCaptureOnCall ()
	{
		StartCoroutine (PrepareToCapture ());
	}

	/// <summary>
	/// open rating page and display Image (if necessary)
	/// </summary>
	public void TowardPageThree ()
	{
		captureAndShareOnClickButtonClass.isCaptureFinish = false;
		buttonEffect.isRotationFinish = false;
		appPage [0].SetActive (false);
		appPage [1].SetActive (false);
		appPage [2].SetActive (true);
		if (flashStart == null) {
			flashStart = StartCoroutine (OnFlashStar());
		} else {
			StopCoroutine (flashStart);
			flashStart = null;
			flashStart = StartCoroutine (OnFlashStar());

		}
		//appPage [3].SetActive (false);
		uiRoot.SetActive (true);
		//ExtractImageFromLocalPathForDisplay ();
	}

	/// <summary>
	/// Extracts the image from local path for display.
	/// </summary>
	private void ExtractImageFromLocalPathForDisplay ()
	{
		bool check = File.Exists (pathForReference);
		Debug.Log ("Extractimage : " + check + " with folloing path" + pathForReference);
		if (File.Exists (pathForReference)) 
		{
			Debug.Log ("open");
			StartCoroutine (getLocalFile(pathForReference));
		}
	}


	private IEnumerator OnClickCamTowardPageTwo ()
	{
		buttonEffect.isRotationFinish = false;
		yield return new  WaitForSeconds (0.1f);
		appPage [0].SetActive (false);
		appPage [1].SetActive (true);
		appPage [2].SetActive (false);
		//appPage [3].SetActive (false);
		UIButton onClickCall = appPage [1].transform.FindChild ("UITexture_BGCAM").GetComponent<UIButton> ();
		EventDelegate.Execute (onClickCall.onClick);
	}

	private IEnumerator PrepareToCapture ()
	{

		buttonEffect.isAnimationFinish = false;
		buttonEffect.isRotationFinish = false;
		yield return null;
		captureAndShareOnClickButtonClass.OnClickCapture ();
	} 

	/// <summary>
	/// Resets all necessary variable.
	/// </summary>
	private void ResetAllNecessaryVariable ()
	{
		captureAndShareOnClickButtonClass.isShareFinish = false;
		captureAndShareOnClickButtonClass.isCaptureFinish = false;
		captureAndShareOnClickButtonClass.isCaptureClicked = false;
		captureAndShareOnClickButtonClass.isShareClicked = false;
		buttonEffect.isRotationFinish = false;
		isRated = false;

	}



	#region onClick Button
	/// <summary>
	/// Raises the click rating event.
	/// </summary>
	/// <param name="rating">Rating.</param>
	public void OnClickRating (GameObject rating)
	{
		//if (!isRated) {
			//appPage [0].SetActive (false);
			//appPage [1].SetActive (false);
			//appPage [2].SetActive (false);
			//appPage [3].SetActive (true);
			OnRateClick(int.Parse(rating.name));
			//WriteData (rating.name);
	
			//DeletePathForfileImagePath ();
			//isRated = true;
		//}

	}

	public IEnumerator OnFlashStar ()
	{
		while (true) {
			yield return new WaitForSeconds (0.2f);
			foreach (buttonConstructor rating in rateTexture) {
				if (rating.thisTexture.alpha == 0.5f) {
					rating.thisTexture.alpha = 0.8f;
				} else {
					rating.thisTexture.alpha = 0.5f;
				}
			}
		}

	}

	public void OnRateClick (int value ){

		if (flashStart != null) {
			StopCoroutine (flashStart);
			flashStart = null;
		}
		shareButton.SetActive (true);
		rate = value.ToString ();
		Debug.Log (value);
		for(int i = 0 ; i < rateTexture.Count; i ++)
		{
			if (i < value) {
				Debug.Log (i);
				rateTexture [i].buttonColour.hover.a = 1f;
				rateTexture [i].buttonColour.pressed.a = 1f;
				rateTexture [i].thisTexture.alpha = 1f;
			} else {
				rateTexture [i].buttonColour.hover.a = 0.5f;
				rateTexture [i].buttonColour.pressed.a = 0.5f;
				rateTexture [i].thisTexture.alpha = 0.5f;

			}
			//} else {

				//rateTexture [i].alpha = 0.5f;
			//}

		}

	}

	/// <summary>
	/// Deletes the path for file image path.
	/// </summary>
/*	public void DeletePathForfileImagePath()
	{
		bool check = File.Exists (fileImagePath);
		Debug.Log ("is file exist : " + check + " with folloing path" + fileImagePath);
		if (File.Exists (fileImagePath)) 
		{
			ReadAndDeletePath ();
		}

	}*/

	#endregion

	#region file Create Delete and Read

	/// <summary>
	/// Reads file from text file then delete path and clear text.
	/// </summary>
	/*private void ReadAndDeletePath ()
	{
		List<string> data = ReadPath(fileImagePath);
		foreach (string path in data) {
			try {
				Debug.Log("reading from text file from following path : " +  fileImagePath + " delete Image From following Path : " + path);
			File.Delete (path);
			} catch (System.Exception ex)
			{
				Debug.Log (ex);
			}
		}
		File.WriteAllText (fileImagePath, "");
	}*/
		
	/// <summary>
	/// Sets up rating text file and folder with file name.
	/// </summary>
	/// <param name="_fileName">File name.</param>
	private void SetUpFile (string _fileName)
	{
		fileName = _fileName;
		directionPath = Path.Combine(Application.persistentDataPath, folderName);
		filePath = Path.Combine(directionPath, fileName);
		fileImagePath = Path.Combine(directionPath, "Image");
		Debug.Log("Current File Path will set as following : " + filePath);
		if (!Directory.Exists(directionPath))
		{
			Directory.CreateDirectory(directionPath);
		}
		if (File.Exists(filePath) )
		{
			return;
		}
		else
		{
			File.Create(filePath).Dispose();

		}
	}

	/// <summary>
	/// Creats the text file to save path and close it.
	/// </summary>
	private void CreatPathFile()
	{
		if (!File.Exists (fileImagePath)) 
		{
			File.Create (fileImagePath).Dispose();
			Debug.Log ("create File path for image path " + fileImagePath);
		}

	}

	/// <summary>
	/// Writes the image path to text
	/// </summary>
	/// <param name="path">Path.</param>
	public void WritePath (string path)
	{
		Debug.Log ("write current path " + path  + " to " + fileImagePath);
		pathForReference = path;
		File.AppendAllText (fileImagePath, path + Environment.NewLine);
	}

	/// <summary>
	/// return all image path from text
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="path">Path.</param>
	public List<string> ReadPath(string path)
	{
		List<string> data = new List<string>();
		StreamReader reader = new StreamReader(path);
		string line = reader.ReadLine();
		while (line != null)
		{
			data.Add(line);
			line = reader.ReadLine();
		}
		reader.Close();
		return data;
	}

	/// <summary>
	/// Writes all rating data in text.
	/// </summary>
	/// <param name="rate">Rate.</param>
	public void WriteData(/*string rate*/)
	{
		Debug.Log ("write rating data : " + rate);
		string period = System.DateTime.Now.ToString ("yyyy/MM/dd HH:mm:ss");
		string input = rate + " : " + period;
		File.AppendAllText (filePath, input + Environment.NewLine);

	}
		
	/// <summary>
	/// Gets the local image from www.
	/// this function used to get capture image to display in screen
	/// </summary>
	/// <returns>The local file.</returns>
	/// <param name="filePath">File path.</param>
	private IEnumerator getLocalFile(string filePath)
	{
		string addInLocalDirection = "file://";
		string fullPath = addInLocalDirection + filePath;
		WWW www = new WWW(fullPath);
		Debug.Log (fullPath);
		yield return www;
		if (string.IsNullOrEmpty (www.error)) {
			imageDisplay.mainTexture = www.texture;
			imageDisplay.width = (int)(imageDisplay.mainTexture.width / 2);
			imageDisplay.height = (int)(imageDisplay.mainTexture.height / 2);
		} else {
			Debug.Log (www.error);
		}
	}
	#endregion




}

[System.Serializable]
public class buttonConstructor {

	public UIButtonColor buttonColour;
	public UITexture thisTexture;
	public UIButton button;

}
