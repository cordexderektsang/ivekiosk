﻿using UnityEngine;
using System.Collections;

public class arrowMovement : MonoBehaviour {

	public float startPoint;
	public float endPoint;
	public float moveDistance = 1;
	private bool start = false;
	[Header("Fade Varable")]
	[Header("speed for fading")]
	public float fadeSpeed;//10
	[Header("increment value for fading")]
	public float fadeColorIncrementValue;//1
	// Use this for initialization
	void Start ()
	{
		startPoint = this.transform.localPosition.y;
	}

	void OnEnable () 
	{
		StartCoroutine (moveArrow ());
	}
		

	public IEnumerator moveArrow ()
	{
		while (true) {
			yield return null;
			float nextPoint = (this.transform.localPosition.y - (moveDistance * Time.deltaTime));
	
			if (this.transform.localPosition.y > endPoint ) {
				this.transform.localPosition = new Vector2 (this.transform.localPosition.x, nextPoint);

			} else {
				this.transform.localPosition = new Vector2 (this.transform.localPosition.x, startPoint);
				nextPoint = 0;
				StartCoroutine(fading (this.gameObject));
			}
		}

	}


	private IEnumerator fading (GameObject targetButton)
	{
		UISprite thisSprite;
		UITexture thisTexture ;
		float color;
		if (targetButton.GetComponent<UISprite> () != null) 
		{

			thisSprite = targetButton.GetComponent<UISprite> ();
			thisSprite.color = new Color (1, 1, 1, 0);
			color = thisSprite.color.a;
			while (thisSprite.color.a < 1) 
			{
				yield return new  WaitForSeconds (fadeSpeed);
				thisSprite.color = new Color (1, 1, 1, color);
				color +=fadeColorIncrementValue;
			}

		} else if (targetButton.GetComponent<UITexture> () != null )
		{
			thisTexture = targetButton.GetComponent<UITexture> ();
			thisTexture.color = new Color (1, 1, 1, 0);
			color = thisTexture.color.a;
			while (thisTexture.color.a < 1) 
			{
				yield return new  WaitForSeconds (fadeSpeed);
				thisTexture.color = new Color (1, 1, 1, color);
				color += fadeColorIncrementValue;
			}

		}


	}
}
