﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class MailClass : MonoBehaviour {

	public Texture2D PhotoTaken;
	public GameObject arCam;
	public string path;
	private byte[] bytes;

	public void takeScreenShot ()
	{

		StartCoroutine (TakeScreenShot());

	}


	// return file name
	string fileName(int width, int height)
	{
		return string.Format("screen_{0}x{1}_{2}.png",
			width, height,
			System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
	}

	public IEnumerator TakeScreenShot()
	{
		yield return new WaitForEndOfFrame();

		Camera camOV = arCam.GetComponent<Camera>();

		RenderTexture currentRT = RenderTexture.active;

		RenderTexture.active = camOV.targetTexture;
		camOV.Render();
		PhotoTaken = new Texture2D(camOV.targetTexture.width, camOV.targetTexture.height, TextureFormat.RGB24, false);
		PhotoTaken.ReadPixels(new Rect(0, 0, camOV.targetTexture.width, camOV.targetTexture.height), 0, 0);
		PhotoTaken.Apply();
		RenderTexture.active = currentRT;


		// Encode texture into PNG
		 bytes = PhotoTaken.EncodeToPNG();

		// save in memory
		//string filename = "picture";


		path = Application.persistentDataPath + "/Snapshots.PNG";

		System.IO.File.WriteAllBytes(path, bytes);

		yield return new WaitForEndOfFrame();
		//StartCoroutine (SendEmail ());
		//sendMailUnity();
		//StartCoroutine (sendMailUnity ());

		// Upload to a cgi script    
		StartCoroutine(UploadFileCo());

	
	
	}

	IEnumerator UploadFileCo()
	{
		string url = "10.0.0.2/kau/ARAlphabetWebsite/iveKioskEmail.php";
		WWWForm postForm = new WWWForm();

		postForm.AddField("name","Derekss");
		postForm.AddField("phone","testingFromUnitys22");
		postForm.AddField("email","tsangderek503789@gmail.com");
		postForm.AddField("message","sss");
		postForm.AddBinaryData("image",bytes);

		WWW upload = new WWW(url,postForm);        
		yield return upload;
		if (upload.error == null)
			Debug.Log("upload done :" + upload.text);
		else
			Debug.Log("Error during upload: " + upload.error);
	}


	public IEnumerator SendEmail()
	{


		string url = "10.0.0.2/kau/ARAlphabetWebsite/iveKioskEmail.php";
		WWWForm form = new WWWForm();

		form.AddField("name","Derekss");
		form.AddField("phone","testingFromUnitys22");
		form.AddField("email","tsangderek503789@gmail.com");
		form.AddField("message","sss");
		form.AddField("path",path);

		WWW www = new WWW(url,form);
		yield return www;
		if (www.error == null) {
			Debug.Log(www.data);
		} else {
			Debug.Log("Error! (" + www.error + ")");
		}

	}

	public IEnumerator sendMailUnity ()
	{
			yield return null;
			string _sender = "cordex";
			string _password = "QdevC2016";
			Debug.Log ("send");
				//For File Attachment, more files can also be attached
				Attachment att = new Attachment(path);
				//tested only for files on local machine



				//Hardcoded recipient email and subject and body of the mail
				string recipient = "derek";
				string subject = "Test Mail";
				string message = "Hello World";

				SmtpClient client = new SmtpClient("mail.smtp2go.com");
				//SMTP server can be changed for gmail, yahoomail, etc., just google it up


				client.Port = 465;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.UseDefaultCredentials = false;
				System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_sender, _password);
				client.EnableSsl = true;
				client.Credentials = (System.Net.ICredentialsByHost)credentials;

				try
				{
					MailMessage mail = new MailMessage();
					mail.From = new MailAddress("News");
					mail.To.Add("tsangderek503789@gmail.com");
					mail.Subject = subject;
					mail.Body = "hi";
					mail.Attachments.Add(att);
					Debug.Log("Attachment is now Online");
					ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
					{
						return true;
					};

					client.Send(mail);
					Debug.Log("Success");
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					throw ex;
				}
			}

}
