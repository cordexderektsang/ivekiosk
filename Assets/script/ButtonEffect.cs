﻿using UnityEngine;
using System.Collections;

public class ButtonEffect : MonoBehaviour {

	public enum ButtonEffectSelection {Scale, CountDown , Rotation, Shake, Normal};

	public AppFlowManager appFlowManager;
	// Use this for initialization

	[Header("Scale Varable")]
	public float scaleX = 0.3f;
	public float scaleY = 0.3f;
	public float scaleTime = 1f;

	[Header("Rotation Varable")]
	public float rotateX = 0.3f;
	public float rotateY = 0.3f;
	public float rotateTime = 1f;



	[Header("Position Varable")]
	public float positionX = 0.3f;
	public float positionY = 0.3f;
	public float positionTime = 1f;

	[Header("Rotate Varable")]
	[Header("speed for each rotation")]
	public float rotateSpeed;//10
	[Header("number of rotation")]
	public int rotateExecuteTime;
	public bool isRotationFinish = false;

	[Header("Fade Varable")]
	[Header("speed for fading")]
	public float fadeSpeed;//10
	[Header("increment value for fading")]
	public float fadeColorIncrementValue;//1


	[Header("playAnimationTime")]
	public float playAnimationTime;//1
	public bool isAnimationFinish = false;

	[Header("effect")]
	public Texture effect;//1
	public GameObject uiRoot;//1
	public GameObject spawnEffect;//1
	public int numberSpawnEffect;//1
	public float numberofTime;//1


	
	// Update is called once per frame



	public void rotation (GameObject targetButton)
	{

		StartCoroutine (rotating ( targetButton));
	}
		

	public void fade (GameObject targetButton)
	{

		StartCoroutine (fading ( targetButton));
	}

	public void playAnimation (UISprite targetButton ,GameObject targetRotation = null)
	{
		StartCoroutine (playingAnimation ( targetButton,targetRotation));
	}

	public void effectOne (GameObject targetButton)
	{

		StartCoroutine (explore ( targetButton));
	}



	public void shakePosition (GameObject targetButton)
	{

		iTween.ShakePosition(targetButton,iTween.Hash("x",positionX,"y",positionY,"time",positionTime));
	}

	public void shakeRotation (GameObject targetButton)
	{

		iTween.ShakeRotation(targetButton,iTween.Hash("x",rotateX,"y",rotateY,"time",rotateTime));
	}

	public void shakeScale (GameObject targetButton)
	{
		iTween.ShakeScale(targetButton,iTween.Hash("x",scaleX,"y",scaleY,"time",scaleTime));
	}



	public IEnumerator playingAnimation (UISprite targetButton , GameObject targetRotation = null)
	{

		isAnimationFinish = false;
		UIAtlas data = targetButton.atlas;
		int i = 0;
		int maxCount = data.spriteList.Count;
		foreach (UISpriteData spriteName in data.spriteList)
		{
			targetButton.spriteName = spriteName.name;
		//	rotation (targetRotation);
			yield return new  WaitForSeconds (playAnimationTime);
			i++;
		}
		if (i == maxCount) {
			yield return new  WaitForSeconds (playAnimationTime);

			isAnimationFinish = true;

		}
			
	}




	private IEnumerator explore (GameObject targetButton)
	{
		Vector3 position = targetButton.transform.localPosition;
		int currentSpawn = 0;

		while (currentSpawn < numberSpawnEffect) 
		{

			yield return new  WaitForSeconds (numberofTime);
			GameObject effect = Instantiate (spawnEffect);
			effect.transform.parent = uiRoot.transform;
			effect.transform.localPosition = position;
			StartCoroutine (moveRandom(effect));
			currentSpawn++;
		}

	}

	private IEnumerator moveRandom (GameObject targetButton)
	{
		int implementedTime = 5;
		int implementCount = 0;
		float[] objects = new float[4];;
		objects[0] = 10f;
		objects[1] = 5f;
		objects[2] = -5f;
		objects[3] = -10f;
		int i = Random.Range (0, 4);
		int j = Random.Range (0, 4);
		while (implementCount <= implementedTime) 
		{

			yield return new  WaitForSeconds (0.01f);
	
			targetButton.transform.localPosition = new Vector2 (targetButton.transform.localPosition.x + objects[i],targetButton.transform.localPosition.y + objects[j]);
		
			if (implementCount == implementedTime) 
			{
				DestroyImmediate (targetButton);
			}
			implementCount ++;

		}

	}

	public void rotationWithAnimationCall (GameObject targetButton, UISprite targetButtonSprite)
	{

		StartCoroutine (rotatingWithAnimation ( targetButton,targetButtonSprite));
	}


	private IEnumerator rotatingWithAnimation (GameObject targetButton , UISprite targetButtonSprite)
	{

		int implementedTime = 0;
		isRotationFinish = false;
		float rotateSpeedReferece = rotateSpeed;
		UIAtlas data = targetButtonSprite.atlas;
		int maxCount = data.spriteList.Count;
		int i = 0;
		foreach (UISpriteData spriteName in data.spriteList) 
		{
			targetButtonSprite.spriteName = spriteName.name;
			while (implementedTime < rotateExecuteTime) 
			{

				yield return new  WaitForSeconds (0.01f);
				float currentRotation = targetButton.transform.localEulerAngles.x + rotateSpeedReferece;
				if (currentRotation >= 360) {
					implementedTime += 1;
					if (implementedTime >= rotateExecuteTime) 
					{
						isRotationFinish = true;
						rotateSpeedReferece = rotateSpeed;
						i++;
				
					}
				
				}
				targetButton.transform.localEulerAngles = new Vector3 (0, 0, currentRotation);
				rotateSpeedReferece += rotateSpeed;
	
			}
			yield return new  WaitForSeconds (0.5f);
			implementedTime = 0;

		}

		if (i == maxCount) 
		{
			yield return new  WaitForSeconds (0.1f);
			appFlowManager.PrepareCaptureOnCall ();
			isAnimationFinish = true;
		}
			
	}

	private IEnumerator rotating (GameObject targetButton)
	{
		int implementedTime = 0;
		isRotationFinish = false;
		float rotateSpeedReferece = rotateSpeed;
		while (implementedTime < rotateExecuteTime) 
		{
			
			yield return new  WaitForSeconds (0.01f);
			float currentRotation = targetButton.transform.localEulerAngles.x + rotateSpeedReferece;
			if (currentRotation >= 360) {
				implementedTime += 1;
				if(implementedTime >=  rotateExecuteTime) 
				{
					appFlowManager.OnEnableCamToPageTwo ();
					isRotationFinish = true;
				}
				rotateSpeedReferece = rotateSpeed;
			}
			targetButton.transform.localEulerAngles = new Vector3 (0,0,currentRotation);
			rotateSpeedReferece += rotateSpeed;

		}

	}


	private IEnumerator fading (GameObject targetButton)
	{
		UISprite thisSprite;
		UITexture thisTexture ;
		float color;
		if (targetButton.GetComponent<UISprite> () != null) 
		{

			thisSprite = targetButton.GetComponent<UISprite> ();
			thisSprite.color = new Color (1, 1, 1, 0);
			color = thisSprite.color.a;
			while (thisSprite.color.a < 1) 
			{
				yield return new  WaitForSeconds (fadeSpeed);
				thisSprite.color = new Color (1, 1, 1, color);
				color +=fadeColorIncrementValue;
			}

		} else if (targetButton.GetComponent<UITexture> () != null )
		{
			thisTexture = targetButton.GetComponent<UITexture> ();
			thisTexture.color = new Color (1, 1, 1, 0);
			color = thisTexture.color.a;
			while (thisTexture.color.a < 1) 
			{
				yield return new  WaitForSeconds (fadeSpeed);
				thisTexture.color = new Color (1, 1, 1, color);
				color += fadeColorIncrementValue;
			}

		}


	}





}
