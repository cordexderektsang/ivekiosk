﻿using UnityEngine;
using System.Collections;

public class MovingBackGround : MonoBehaviour {

	MeshRenderer mr;
	Material mat;

	void Start (){

		mr = GetComponent<MeshRenderer> ();

	}
	
	// Update is called once per frame
	void Update () {

		mat = mr.material;
		Vector2 offset = mat.mainTextureOffset;
		offset.y += Time.deltaTime/10;
		mat.mainTextureOffset = offset;

	}
}
