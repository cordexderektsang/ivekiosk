package com.cordex.saveorshare;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.GLES20;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by cordexhason on 14/9/15.
 */
public class SaveOrShare {

    private static final String FACEBOOK_APP_NAME = "com.facebook.composer.shareintent";
    private static final String FACEBOOK_APP_PACKAGE_NAME = "com.facebook.katana";

    private static final String TWITTER_APP_NAME = "com.twitter.android.composer.ComposerActivity";
    private static final String TWITTER_APP_PACKAGE_NAME = "com.twitter.android";

    private static final String WEIBO_APP_NAME = "com.sina.weibo.composerinde.ComposerDispatchActivity";
    private static final String WEIBO_APP_PACKAGE_NAME = "com.sina.weibo";

    /**
     * Converts byte[] image representation into an actual file.
     *
     * @param activity     the current activity
     * @param picByteArray the byte[] array representation of a new picture
     * @param folder       the subfolder within Android Pictures folder to store
     * @param mimeType     mimeType to be scanned
     * @return the file and file path of the image saved
     */
    public static String SaveBitmapToImage(Activity activity, byte[] picByteArray, String folder, String mimeType, UnityCallback callbackClass) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss.SSS", Locale.US);
        Calendar cal = Calendar.getInstance();
        String now = fmt.format(cal.getTime());
        String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + folder;
        String fileWithPath = filePath + "/" + now + "." + mimeType;
        Bitmap bmp;

        // Create subfolder first
        CreateAppPictureFolder(filePath);

        // If input byte[] is null, it implies a screenshot has to be taken via native code
        if (picByteArray == null || picByteArray.length == 0) {
            bmp = takeScreenshot(0, 0, activity.getResources().getDisplayMetrics().widthPixels, activity.getResources().getDisplayMetrics().heightPixels, callbackClass);
        } else {
            bmp = BitmapFactory.decodeByteArray(picByteArray, 0, picByteArray.length);
        }

        File imageFile = new File(fileWithPath);
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(imageFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        bmp.compress(Bitmap.CompressFormat.PNG, 75, fOut);
        try {
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Notify callback capture is complete
        callbackClass.CaptureComplete(1);

        return fileWithPath;
    }

    /**
     * Takes a screenshot of the current activity
     *
     * @param activity the current activity
     * @return the byte[] array of the screenshot
     */
    public static byte[] takeScreenshot(Activity activity, UnityCallback callbackClass) {
        // Create bitmap of screen capture
        View v1 = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);

        return stream.toByteArray();
    }

    public static Bitmap takeScreenshot(int x, int y, int w, int h, UnityCallback callbackClass) {
        int b[] = new int[w * (y + h)];
        int bt[] = new int[w * h];
        IntBuffer ib = IntBuffer.wrap(b);
        ib.position(0);

        GLES20.glReadPixels(x, 0, w, y + h,
                GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, ib);

        for (int i = 0, k = 0; i < h; i++, k++) {
            for (int j = 0; j < w; j++) {
                int pix = b[i * w + j];
                int pb = (pix >> 16) & 0xff;
                int pr = (pix << 16) & 0x00ff0000;
                int pix1 = (pix & 0xff00ff00) | pr | pb;
                bt[(h - k - 1) * w + j] = pix1;
            }
        }

        Bitmap sb = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);

        return sb;
    }

    /*
    *//**
     * Performs a media scan for a given image object filename
     *
     * @param activity      the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     *//*
    public static void RescanMedia(Activity activity, String fileWithPath, String mimeType) {
//        System.out.println("Rescan media for file");
        File file = new File(fileWithPath);
        MediaScannerConnection.scanFile(
                activity,
                new String[]{file.getAbsolutePath()},
                new String[]{mimeType},
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
//                        System.out.println("Finished rescanning media for file");
                    }
                });
    }*/

    /**
     * Performs a media scan for a given image object filename
     *
     * @param activity     the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     */
    public static void RescanMedia(Activity activity, String fileWithPath, String mimeType) {
        File file = new File(fileWithPath);

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
        values.put(MediaStore.Images.Media.MIME_TYPE, mimeType);
        activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    /**
     * Share the image file for a given image object filename
     *
     * @param activity     the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     */
    public static void ShareImage(final Activity activity, final String fileWithPath, final String mimeType) {
        final File file = new File(fileWithPath);
        RescanMedia(activity, fileWithPath, mimeType);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType(mimeType);
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        activity.startActivity(Intent.createChooser(share, "Share Image"));
        /*MediaScannerConnection.scanFile(
                activity,
                new String[]{file.getAbsolutePath()},
                new String[]{mimeType},
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        if (mimeType == null || mimeType.equals("")) {
                            share.setType("image*//*");
                        } else {
                            share.setType(mimeType);
                        }
                        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                        activity.startActivity(Intent.createChooser(share, "Share Image"));
                    }
                });*/
    }

    /**
     * Share the image file to Facebook for a given image object filename
     *
     * @param activity     the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     * @param message      Message passed in to share on the post
     */
    public static void ShareImageToFacebook(final Activity activity, final String fileWithPath,
                                            final String mimeType, final String message) {
        ShareImageToSocial(activity, fileWithPath, mimeType, message, FACEBOOK_APP_NAME, FACEBOOK_APP_PACKAGE_NAME);
        /*try {
            List<Intent> targetedShareIntents = new ArrayList<Intent>();
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType(mimeType);
            List<ResolveInfo> resInfo = activity.getPackageManager()
                    .queryIntentActivities(share, 0);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
                    targetedShare.setType(mimeType);
                    if (info.activityInfo.packageName.contains("com.facebook.katana")
                            && info.activityInfo.name.contains("com.facebook.composer.shareintent")) {
//                        targetedShare.putExtra(Intent.EXTRA_SUBJECT,
//                                "Sample Photo");
//                        targetedShare.putExtra(Intent.EXTRA_TEXT, "Sample Message");
                        targetedShare.putExtra(Intent.EXTRA_STREAM,
                                Uri.fromFile(new File(fileWithPath)));
                        targetedShare.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                        targetedShareIntents.add(targetedShare);
                    }
                }
                Intent chooserIntent = Intent.createChooser(
                        targetedShareIntents.remove(0), "Select app to share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                        targetedShareIntents.toArray(new Parcelable[]{}));
                activity.startActivity(chooserIntent);
            }
        } catch (Exception e) {
            Log.e("SaveOrShare", "Exception while sending image " + e.getMessage());
        }*/
    }

    /**
     * Share the image file to Twitter for a given image object filename
     *
     * @param activity     the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     * @param message      Message passed in to share on the post
     */
    public static void ShareImageToTwitter(final Activity activity, final String fileWithPath,
                                           final String mimeType, final String message) {
        ShareImageToSocial(activity, fileWithPath, mimeType, message, TWITTER_APP_NAME, TWITTER_APP_PACKAGE_NAME);
        /*try {
            List<Intent> targetedShareIntents = new ArrayList<Intent>();
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType(mimeType);
            List<ResolveInfo> resInfo = activity.getPackageManager()
                    .queryIntentActivities(share, 0);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    Log.d("SaveOrShare", info.activityInfo.packageName + ", " + info.activityInfo.name);
                }

                for (ResolveInfo info : resInfo) {
                    Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
                    targetedShare.setType(mimeType);
                    if (info.activityInfo.packageName.contains("com.twitter.android")
                            && info.activityInfo.name.contains("com.twitter.android.composer.ComposerActivity")) {
                        targetedShare.putExtra(Intent.EXTRA_SUBJECT,
                                "Sample Photo");
                        targetedShare.putExtra(Intent.EXTRA_TEXT, "Sample Message");
                        targetedShare.putExtra(Intent.EXTRA_STREAM,
                                Uri.fromFile(new File(fileWithPath)));
//                        targetedShare.setPackage(info.activityInfo.packageName);
                        targetedShare.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                        targetedShareIntents.add(targetedShare);
                        break;
                    }
                }
                Intent chooserIntent = Intent.createChooser(
                        targetedShareIntents.remove(0), "Select app to share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                        targetedShareIntents.toArray(new Parcelable[]{}));
                activity.startActivity(chooserIntent);
            }
        } catch (Exception e) {
            Log.e("SaveOrShare", "Exception while sharing to twitter " + e.getMessage());
        }*/
    }

    /**
     * Share the image file to Weibo for a given image object filename
     *
     * @param activity     the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     * @param message      Message passed in to share on the post
     */
    public static void ShareImageToWeibo(final Activity activity, final String fileWithPath,
                                         final String mimeType, final String message) {
        ShareImageToSocial(activity, fileWithPath, mimeType, message, WEIBO_APP_NAME, WEIBO_APP_PACKAGE_NAME);
    }

    /**
     * Share the image file to an external application for a given image object filename
     *
     * @param activity     the {@link Activity} of the current running application
     * @param fileWithPath file path of image file to be scanned
     * @param mimeType     mimeType to be scanned
     * @param message      Message passed in to share on the post
     * @param appName      Name of social media platform
     * @param packageName  Package name of social media platform
     */
    public static void ShareImageToSocial(final Activity activity, final String fileWithPath,
                                           final String mimeType, final String message,
                                           final String appName, final String packageName) {
        try {
            List<Intent> targetedShareIntents = new ArrayList<Intent>();
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType(mimeType);
            List<ResolveInfo> resInfo = activity.getPackageManager()
                    .queryIntentActivities(share, 0);
            if (!resInfo.isEmpty()) {
                /*for (ResolveInfo info : resInfo) {
                    Log.d("SaveOrShare", info.activityInfo.packageName + ", " + info.activityInfo.name);
                }*/

                for (ResolveInfo info : resInfo) {
                    Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
                    targetedShare.setType(mimeType);
                    if (info.activityInfo.packageName.contains(packageName)
                            && info.activityInfo.name.contains(appName)) {
                        targetedShare.putExtra(Intent.EXTRA_TEXT, message);
                        targetedShare.putExtra(Intent.EXTRA_STREAM,
                                Uri.fromFile(new File(fileWithPath)));
                        targetedShare.setClassName(info.activityInfo.packageName,
                                                    info.activityInfo.name);
                        targetedShareIntents.add(targetedShare);
                        break;
                    }
                }
                Intent chooserIntent = Intent.createChooser(
                        targetedShareIntents.remove(0), "Select app to share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                        targetedShareIntents.toArray(new Parcelable[]{}));
                activity.startActivity(chooserIntent);
            }
        } catch (Exception e) {
            Log.e("SaveOrShare", "Exception while sharing to " + appName + ": " + e.getMessage());
        }
    }

    /**
     * Creates the directory under Android Pictures folder for this application
     *
     * @param folder the folder to be created
     */
    private static void CreateAppPictureFolder(String folder) {
        File f = new File(folder);
        try {
            f.mkdir();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface UnityCallback {
        public void CaptureComplete(int result);
    }
}