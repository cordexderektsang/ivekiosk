#include "UI/UnityViewControllerBase.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface CCCaptureAndShare : NSObject

//UIActivityViewController
- (void)StartSharingImage: (UIImage *)imageToShare
                    title:(NSString *)social_type
                      msg:(NSString *)textToShare;

//SLComposeViewController

- (void)StartSharingImageToSocial:(UIImage *)imageToShare
                             type:(NSString *)social_type
                            title:(NSString *)titleToShare
                              msg:(NSString *)textToShare;

// Save Image To Album
- (void)saveImage:(UIImage *)image;

// Callback of save image function
- (void)imageSaveCallback       : (UIImage *)image
        didFinishSavingWithError: (NSError *)error
        contextInfo             : (void *)contextInfo;

- (UIImage *)captureScreen;
@end